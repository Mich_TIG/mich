# About/A propos de moi...

## Centres d'intérêts

Cupcake indexer is a snazzy new project for indexing small cakes.

![Screenshot](img/diag.png)

*Above: Cupcake indexer in progress*

### With tags 🏷️

## Coralie
- [x] 10/2018: Thunderbird Bug

- [x] this is a complete item
- [    ] this is an incomplete item
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported- [x] list syntax required (any unordered or ordered list supported)
- GitHub supports emoji !
:+1: :sparkles: :camel: :tada: :rocket: :metal: :octocat:
- 
```
code fences
```

```js
codeFences.withLanguage()

| Column 1 Heading | Column 2 Heading |
| ---------------- | ---------------- |
| Some content     | Other content    |

## Admonitions

The following admonitions are formatted like the callouts above but can be implemented in Markdown when you include the `admonition` Markdown extension in your `mkdocs.yml` file.  

Add the following setting to `mkdocs.yml`:

```yaml
markdown_extensions:
  - admonition
```

1. raisin
0. pomme
 ⟼ * golden
 ⟼ - granny smith
 ⟼ + boskoop
0. abricot

1291\. Signature du pacte fédéral Suisse

Utilisation de balises HTML : <u>souligné</u>,
  <span style="color: #f00;">couleur</span> ...

<table border="1" cellspacing="0" cellpadding="4" align="center">
   <tr> <td>Tableau</td> <td>par balises HTML</td> </tr> 
</table>

Paragraphe _normal_...